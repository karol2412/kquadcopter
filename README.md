# kquadcopter

This repository is meant to contain my quadcopter SW based on nrf52 quadcopter project.

HW is the same as in project described by the post https://devzone.nordicsemi.com/nordic/nordic-blog/b/blog/posts/nrf52-quadcopter

Mechanics will be similar (there will be some necessary improvements).

Software will be based on Zephyr RTOS.
